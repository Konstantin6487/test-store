import React from "react";
import { connect } from 'react-redux';

class Home extends React.Component {
  render() {
    return <div>Companies</div>;
  }
}


const mapStateToProps = state => {
  const props = {
    companies: state.companies,
  }
  return props;
};

export default connect(mapStateToProps)(Home);
