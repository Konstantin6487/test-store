import { combineReducers } from "redux";
import { handleActions } from "redux-actions";
import { omit } from "lodash";
import { addCompany, removeCompany, addUnit, addEmployer, removeEmployer } from "../actions";
import update from "immutability-helper";


const companies = handleActions({

  [addCompany](state, { payload } ) {
    let newId = 0;
    if (Object.keys(state).length === 0) {
      newId += 1;
    }
    else {
      newId = Math.max(...Object.keys(state).map(Number)) + 1;
    }
    return {...state, [newId]: { id: newId, ...payload } };
  },

  [removeCompany](state, { payload: { id } } ) {
    return omit(state, [id]);
  },

  [addUnit](state, { payload: { title, companyId }}) {
    const updatedCompany = update(state, {
      [companyId]: {
        "units": {
          [title]: {
            $set: {}
          }
        }
      }
    });
    return updatedCompany;
  }

}, {});

export default combineReducers({companies});
