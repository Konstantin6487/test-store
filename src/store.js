import { createStore } from "redux";
import reducers from "./reducers";

const reduxDevtools = window.__REDUX_DEVTOOLS_EXTENSION__;
export default createStore(reducers, reduxDevtools && reduxDevtools());
