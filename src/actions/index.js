import { createAction } from "redux-actions";

export const addCompany = createAction("COMPANY_ADD");
export const removeCompany = createAction("COMPANY_REMOVE");
export const addUnit = createAction("UNIT_ADD");
export const addEmployer = createAction("EMPLOYER_ADD");
export const removeEmployer = createAction("EMPLOYER_REMOVE");
