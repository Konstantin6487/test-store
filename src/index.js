import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import App from "./components/App";
import {
  addCompany,
  removeCompany,
  addUnit,
  addEmployer,
  removeEmployer
} from "./actions";
import store from "./store";

const newCompany = (title, units = {}) => ({ title, units });
const newEpmloyer = (name, companyId) => ({ name, companyId });
const newUnit = (title, companyId) => ({ title, companyId });

store.dispatch(addCompany(newCompany("GRID")));
store.dispatch(addCompany(newCompany("EPAM")));
store.dispatch(addCompany(newCompany("Undev")));
store.dispatch(addEmployer(newEpmloyer("Ivan", 1)));
store.dispatch(addUnit(newUnit("workers", 2)));
store.dispatch(addUnit(newUnit("leadership", 2)));

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
